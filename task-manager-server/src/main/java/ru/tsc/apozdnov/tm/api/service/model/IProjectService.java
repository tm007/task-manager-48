package ru.tsc.apozdnov.tm.api.service.model;

import ru.tsc.apozdnov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

}
